��    8      �  O   �      �  L   �     &     ,     5     D  
   P     [  L   {     �     �     �     �  4   �  !   -     O  8   ^     �     �     �     �     �     �     �     �     �     �  	   	       -         N     [     b     q     y     �     �     �     �     �     �     �  �  �  _   �	     �	     �	     
     
     
     
  (   7
     `
  `   d
     �
  
   �
     �
  j  �
  H   [     �  
   �     �     �     �     �  P   �     N     U     h     u  (   |      �     �  B   �               %  
   8     C  	   P     Z  	   _     i     �  	   �     �  K   �     �  
   �               %     2     ;     B     P     \     j     }  #  �  m   �          "     2  	   9  
   C     N  +   h     �  t   �       
        '            6   0   1      !      *                           
             -             '       %       $          3           2   #          8   (                      .   	                  &   7           /   5   ,              )             4                 +                   "    ...or has never existed. Create a new one at the <a href ="/">main page</a>. Abort About Us About the game Broken Link Contact us Copy the link to your adversary Create a <a href="/new_game/">new match</a> and invite your friends to play. Create! DigGems Patronage Donate Bitcoin Empty! Error 403: Somebody is already playing in this place Error 404: This game has expired! Facebooken_US Find another match to join in the <a href="/">index</a>; Game Game %(game_id)s Games to start How to play In progress Join! Logout New game No game found! Oops! Error 404 Patronage PayPalen_US PayPal - The safer, easier way to pay online! Play it now! Policy Privacy policy Private Private game Public Score Source code Take Player's Turn Terminate Game Terms of service Thanks! This game is free to play and <a href="/info/sourcecode">free software</a>. We also
	don't feel to be fair to charge for competitive advantage of some players over others. Anyway, we have
	dedicated much of our time to develop this game, and have our costs in keeping it running and in its
	continual development. Thus, we shamelessly ask for your patronage: any amount you can spare (greater
	than PayPal transaction fees) will be appreciated. This page does not exist. You may have mis-typed the URL. Go back to <a href="/">main page</a>. US Unauthorized Versus Visitor Wait... Waiting the other player Watch that match <a href="../">here</a>; You You can not play this game anymore, because someone else is already playing it. You may instead: Your adversary Your games anonymous player Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-25 18:05-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 ...ou nunca existiu. Crie um novo na <a href ="/">página principal</a>. Abortar Sobre Nós Sobre o jogo Link Quebrado Contate-nos Copie o link para seu oponente Criar uma <a href="/new_game/">nova partida</a> e chamar seus amigos para jogar. Criar! Suporte ao DigGems Doar Bitcoin Vazio! Erro 403: Alguém já está jogando aqui Erro 404: Este jogo já expirou! pt_BR Encontrar outra partida para se juntar no <a href="/">índice</a>; Jogo Jogo %(game_id)s Jogos por começar Como jogar Em progresso Juntar-se Sair Novo jogo Nenhum jogo encontrado! Oops! Erro 404 Doações pt_BR PayPal - A maneira mais simples e segura de pagar suas compras na Internet! Jogue agora! Políticas Política de privacidade Privado Jogo privado Público Placar Código fonte Tomar a Vez Terminar Jogo Termos do serviço Muito obrigado! Este jogo é gratuito e <a href="/info/sourcecode">software livre</a>. Além disso, nós consideramos injusto cobrar por vantages competitivas que algum jogador possa adquirir sobre outros. De todo modo, nós temos dedicado muito do nosso tempo no desenvolvimento deste jogo, e temos custos tanto para mantê-lo no ar quanto com o seu continuado desenvolvimento. Por isso, não temos receio de pedir por seu apoio financeiro: qualquer quantia que você puder nos dispor (desde que seja maior que as taxas de transação do PayPal) será apreciada. Esta página não existe. Você deve ter digitado a URL errado. Volte para <a href="/">página principal</a>. BR Não autorizado Contra Visitante Aguarde... Esperando o outro jogador Assistir à partida <a href="../">aqui</a>; Você Você não pode mais se juntar a esta partida, porque já tem alguém jogando neste lugar. Em vez disso, você pode: Seu oponente Seus jogos jogador anônimo 