��    +      t  ;   �      �  L   �                    '     G     O     ^  4   e  !   �     �     �     �     �     �     �                         -     =  -   J     x     �     �     �     �     �     �     �  _   �     5     8     E     L     T     \     u     y  
   �     �  k  �  M        ^     f  	   u           �     �     �  (   �     �     	     
	     	     "	  
   5	     @	     I	     Q	     ]	     i	     �	     �	  0   �	     �	     �	     �	     �	     �	     
     
     "
  n   4
     �
     �
     �
  	   �
  	   �
     �
     �
     �
  
   �
                       
      *                      #         !                           &          "                +                                        (   )          '   $         	          %              ...or has never existed. Create a new one at the <a href ="/">main page</a>. Abort About the game Broken Link Copy the link to your adversary Create! Donate Bitcoin Empty! Error 403: Somebody is already playing in this place Error 404: This game has expired! Facebooken_US Game Game %(game_id)s Games to start How to play In progress Join! Logout New game No game found! Oops! Error 404 PayPalen_US PayPal - The safer, easier way to pay online! Play it now! Private Private game Public Score Source code Take Player's Turn Terminate Game This page does not exist. You may have mis-typed the URL. Go back to <a href="/">main page</a>. US Unauthorized Versus Visitor Wait... Waiting the other player You Your adversary Your games anonymous player Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-25 18:05-0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 ...o nunca existió. Crie uno nuevo en la <a href ="/">página principal</a>. Abortar Sobre el juego Link roto Copie este link para tu oponente Crear! Donar Bitcoin Vazio! Error 403: Alguien ya está jugando aqui Error 404: Este juego terminó es_LA Juego Juego %(game_id)s Juegos por empezar Como jugar En curso Unirse! Desconectar Nuevo juego Ningún juego encontrado! Oops! Error 404 es_ES PayPal - La forma más fácil y segura de pagar! ¡Juege ahora! Privado Juego privado Público Pontuación Código fuente Tomar el turno Terminar el juego Esta página no existe. Es posible que haya escrito mal la URL. Vuelva a la <a href="/">página principal</a>. ES No autorizado Versus Visitante Espere... Esperando el otro jugador Usted Tu adversario Sus juegos jugador anónimo 